package fr.fingarde.mineandglory.objects.shop;

import org.bukkit.Material;

public enum BlocksPage implements ShopPage
{
    STONES(Material.STONE, "Stones", 11),
    DIRT(Material.DIRT, "Dirts", 12),
    SAND(Material.SAND, "Sands", 13),
    WOOD(Material.OAK_LOG, "Woods", 14),
    BRICKS(Material.BRICKS, "Bricks", 15),

    WOOL(Material.WHITE_WOOL, "Wools", 19),
    TERRACOTTA(Material.TERRACOTTA, "Terracottas", 20),
    GLASS(Material.GLASS, "Glasses", 21),
    CONCRETE(Material.BLUE_CONCRETE, "Concrete", 22),

    CORAL(Material.BRAIN_CORAL_BLOCK, "Corals", 23),
    ICE(Material.BLUE_ICE, "Ices", 24),
    PRISMARINE(Material.PRISMARINE, "Prismarines", 25),


    NETHER(Material.NETHERRACK, "Nether blocks", 30),
    END(Material.END_STONE, "End blocks", 31);




    BlocksPage(Material material, String title, int pos)
    {
        this.material = material;
        this.title = title;
        this.pos = pos;
    }

    private Material material;
    private String title;
    private int pos;

    @Override
    public Material getMaterial()
    {
        return material;
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public int getPos()
    {
        return pos;
    }
}
