package fr.fingarde.mineandglory.objects.shop;

import org.bukkit.Material;

public interface ShopPage
{
    Material getMaterial();
    String getTitle();
    int getPos();
}
