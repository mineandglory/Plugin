package fr.fingarde.mineandglory.objects.shop;

import org.bukkit.Material;

public enum MainPage implements ShopPage
{
    BLOCK(Material.BRICKS, "Blocks", 20),
    DECORATION(Material.ANVIL, "Decoration", 21),
    POTION(Material.POTION, "Potions", 23),
    REDSTONE(Material.REDSTONE, "Redstone", 24),
    FOOD(Material.APPLE, "Nourriture", 30),
    EQUIPEMENT(Material.IRON_PICKAXE, "Equipement", 31),
    ORE(Material.DIAMOND, "Minerais", 32);

    MainPage(Material material, String title, int pos)
    {
        this.material = material;
        this.title = title;
        this.pos = pos;
    }

    private Material material;
    private String title;
    private int pos;

    @Override
    public Material getMaterial()
    {
        return material;
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public int getPos()
    {
        return pos;
    }
}
