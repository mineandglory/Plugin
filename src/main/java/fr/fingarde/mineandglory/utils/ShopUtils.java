package fr.fingarde.mineandglory.utils;

import fr.fingarde.mineandglory.objects.shop.ShopPage;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ShopUtils
{
    public static <E extends Enum<E> & ShopPage> void setItems(Class<E> eNum, Inventory inventory) {
        EnumUtils.forEach(eNum, e -> {
            ItemStack item = new ItemStack(e.getMaterial());
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§r" + e.getTitle());
            item.setItemMeta(meta);

            inventory.setItem(e.getPos(), item);
        });
    }
}
