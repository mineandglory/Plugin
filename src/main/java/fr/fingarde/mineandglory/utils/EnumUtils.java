package fr.fingarde.mineandglory.utils;

import java.util.function.Consumer;

public class EnumUtils
{
    public static <E extends Enum<E>> void forEach(Class<E> enumClass, Consumer<E> action)
    {
        for (E e : enumClass.getEnumConstants())
            action.accept(e);
    }

    public static <E extends Enum<E>> E next(Class<E> enumClass, E value)
    {
        E[] values = enumClass.getEnumConstants();
        return values[(value.ordinal() + 1) % values.length];
    }
}