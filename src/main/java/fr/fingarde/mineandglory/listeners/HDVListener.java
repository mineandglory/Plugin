package fr.fingarde.mineandglory.listeners;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fr.fingarde.mineandglory.Main;
import fr.fingarde.mineandglory.objects.User;
import fr.fingarde.mineandglory.utils.ColorUtils;
import fr.fingarde.mineandglory.utils.ErrorMessage;
import fr.fingarde.mineandglory.utils.FloatUtils;
import fr.fingarde.mineandglory.utils.HDVUtils;
import fr.fingarde.mineandglory.utils.serializer.ItemSerializer;
import fr.fingarde.mineandglory.utils.storage.Database;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class HDVListener implements Listener
{
    @EventHandler
    public void onClickMenu(InventoryClickEvent event)
    {
        if (!event.getView().getTitle().startsWith("Hotel des ventes - page N°")) return;
        if (event.getCurrentItem() == null) return;

        event.setCancelled(true);
        ((Player) event.getWhoClicked()).updateInventory();

        ItemStack clickedItem = event.getCurrentItem();

        int nextPage = 0;
        if (clickedItem.getItemMeta().getLocalizedName().equals("NEXT")) nextPage++;
        if (clickedItem.getItemMeta().getLocalizedName().equals("PREVIOUS")) nextPage--;

        int page = Integer.parseInt(event.getView().getTitle().split("°")[1]);

        if (nextPage == 0)
        {

            HDVUtils.openItem(clickedItem, (Player) event.getWhoClicked(), page);
            return;
        }

        HDVUtils.openPage(page + nextPage, (Player) event.getWhoClicked());
    }

    @EventHandler
    public void onClickBuy(InventoryClickEvent event)
    {
        if (!event.getView().getTitle().startsWith("Hotel des ventes - Achat")) return;
        if (event.getCurrentItem() == null) return;

        Inventory inventory = event.getClickedInventory();
        Player player = (Player) event.getWhoClicked();

        event.setCancelled(true);
        player.updateInventory();

        ItemStack clickedItem = event.getCurrentItem();
        if (clickedItem.getItemMeta().getLocalizedName().startsWith("CANCEL:"))
        {
            int page = Integer.parseInt(clickedItem.getItemMeta().getLocalizedName().split(":")[1]);

            HDVUtils.openPage(page, player);
            return;
        }

        if (clickedItem.getItemMeta().getLocalizedName().startsWith("SET:"))
        {
            int value = Integer.parseInt(clickedItem.getItemMeta().getLocalizedName().split(":")[1]);
            ItemStack itemSell = inventory.getItem(22);

            ItemStack confirmItem = inventory.getItem(32);
            ItemMeta confirmItemMeta = confirmItem.getItemMeta();
            JsonObject json = new JsonParser().parse(ColorUtils.unhideChars(itemSell.getItemMeta().getLore().get(1))).getAsJsonObject();

            int max = itemSell.getAmount();
            float priceDefault = json.get("price").getAsFloat();

            int newAmount = confirmItem.getAmount() + value;
            if (newAmount < 1) newAmount = 1;
            if (newAmount > max) newAmount = max;

            float newPrice = FloatUtils.scaleDown(newAmount * priceDefault / max);
            json.addProperty("price", newPrice);

            confirmItem.setAmount(newAmount);

            List<String> lore = new LinkedList<>();
            lore.add("§ePrix " + newPrice + "$");
            lore.add(ColorUtils.hideChars(json.toString()));
            confirmItemMeta.setLore(lore);

            confirmItemMeta.setDisplayName("§rAcheter " + newAmount);
            confirmItem.setItemMeta(confirmItemMeta);

            inventory.setItem(32, confirmItem);
            return;
        }

        if (clickedItem.getItemMeta().getLocalizedName().startsWith("BUY"))
        {
            User user = User.getByUUID(event.getWhoClicked().getUniqueId());

            boolean place = false;
            for (ItemStack item : event.getWhoClicked().getInventory().getStorageContents())
            {
                if (item == null)
                {
                    place = true;
                    break;
                }
            }
            if (!place)
            {
                event.getWhoClicked().sendMessage(ErrorMessage.inventoryFull());
                event.getWhoClicked().closeInventory();
                return;
            }

            JsonObject json = new JsonParser().parse(ColorUtils.unhideChars(inventory.getItem(22).getItemMeta().getLore().get(1))).getAsJsonObject();
            float price = json.get("price").getAsFloat();

            if (user.getMoney() < price)
            {
                event.getWhoClicked().sendMessage(ErrorMessage.notEnoughMoney());
                event.getWhoClicked().closeInventory();
                return;
            }

            ItemStack itemSell = inventory.getItem(22);
            ItemMeta meta = itemSell.getItemMeta();

            List<String> lore = meta.getLore();
            lore.remove(0);
            lore.remove(0);

            meta.setLore(lore);
            itemSell.setItemMeta(meta);

            itemSell.setAmount(clickedItem.getAmount());

            new BukkitRunnable()
            {
                @Override
                public void run()
                {
                    try (
                            Connection connection = Database.getSource().getConnection();
                            Statement statement = connection.createStatement();
                            ResultSet resultSet = statement.executeQuery("SELECT * FROM tb_market WHERE mk_id = '" + json.get("id").getAsString() + "'"))
                    {
                        if (!resultSet.next())
                        {
                            player.sendMessage(ErrorMessage.itemAlreadySell());
                            player.closeInventory();
                        }

                        int restant = resultSet.getInt("mk_amount");
                        int nombreAcheter = Math.min(restant, clickedItem.getAmount());

                        player.sendMessage("Vous avez acheter " + nombreAcheter);

                        if (nombreAcheter == restant)
                        {
                            statement.executeUpdate("DELETE FROM tb_market WHERE mk_id = '" + json.get("id").getAsString() + "'");
                        } else
                        {
                            statement.executeUpdate("UPDATE tb_market SET mk_amount = mk_amount - " + nombreAcheter + " WHERE mk_id = '" + json.get("id").getAsString() + "'");
                        }

                        new BukkitRunnable()
                        {
                            @Override
                            public void run()
                            {
                                player.getInventory().addItem(itemSell);
                                player.closeInventory();
                            }
                        }.runTask(Main.getPlugin());
                    } catch (SQLException e)
                    {
                        e.printStackTrace();
                    }
                }
            }.runTaskAsynchronously(Main.getPlugin());
        }
    }
}
