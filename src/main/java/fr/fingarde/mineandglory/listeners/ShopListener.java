package fr.fingarde.mineandglory.listeners;

import fr.fingarde.mineandglory.objects.shop.BlocksPage;
import fr.fingarde.mineandglory.utils.ShopUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ShopListener implements Listener
{
    @EventHandler
    public void onClickBuy(InventoryClickEvent event)
    {
        if (!event.getView().getTitle().startsWith("Shop - Accueil")) return;
        if (event.getCurrentItem() == null) return;

        Inventory inventory = event.getClickedInventory();
        Player player = (Player) event.getWhoClicked();

        event.setCancelled(true);
        player.updateInventory();

        ItemStack clickedItem = event.getCurrentItem();
        if (clickedItem.getItemMeta().getDisplayName().equals("§rBlocks"))
        {
            inventory.clear();
            ShopUtils.setItems(BlocksPage.class, inventory);
            return;
        }
    }
}
