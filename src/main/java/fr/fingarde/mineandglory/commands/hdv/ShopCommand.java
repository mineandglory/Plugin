package fr.fingarde.mineandglory.commands.hdv;

import fr.fingarde.mineandglory.Main;
import fr.fingarde.mineandglory.objects.shop.MainPage;
import fr.fingarde.mineandglory.objects.shop.ShopPage;
import fr.fingarde.mineandglory.utils.ErrorMessage;
import fr.fingarde.mineandglory.utils.FloatUtils;
import fr.fingarde.mineandglory.utils.HDVUtils;
import fr.fingarde.mineandglory.utils.ShopUtils;
import fr.fingarde.mineandglory.utils.serializer.ItemSerializer;
import fr.fingarde.mineandglory.utils.storage.Database;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.UUID;

public class ShopCommand implements CommandExecutor
{
    private static String permission = "command.shop";
    private static String permissionBuy = "command.shop.buy";
    private static String permissionSell = "command.shop.sell";

    private static String usage = "/hdv [sell <price>]";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] arguments)
    {
        if (!(sender instanceof Player))
        {
            sender.sendMessage(ErrorMessage.onlyOnPlayer());
            return false;
        }

        Player player = (Player) sender;
        if (!sender.hasPermission(permission))
        {
            sender.sendMessage(ErrorMessage.noPermissionMessage(permission));
            return false;
        }

        Inventory inventory = Bukkit.createInventory(null, 54, "Shop - Accueil");
        ShopUtils.setItems(MainPage.class, inventory);

        player.openInventory(inventory);
        return false;
    }
}
